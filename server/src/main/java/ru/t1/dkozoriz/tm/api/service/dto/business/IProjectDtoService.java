package ru.t1.dkozoriz.tm.api.service.dto.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;

public interface IProjectDtoService extends IBusinessDtoService<ProjectDto> {

    @NotNull
    ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    );

}