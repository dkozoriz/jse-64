package ru.t1.dkozoriz.tm.api.service.model.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.List;

public interface ITaskService extends IBusinessService<Task> {

    @NotNull String getName();

    @NotNull Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable String projectId
    );

    @NotNull List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    @NotNull Task bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    @NotNull Task unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );
}
