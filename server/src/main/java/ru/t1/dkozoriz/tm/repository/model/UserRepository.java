package ru.t1.dkozoriz.tm.repository.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.model.User;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

}