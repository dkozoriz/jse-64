package ru.t1.dkozoriz.tm.repository;

import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public final class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("Task 1", "description1"));
        add(new Task("Task 2", "description2"));
        add(new Task("Task 3", "description3"));
    }

    public void add(Task Task) {
        tasks.put(Task.getId(), Task);
    }

    public void create() {
        add(new Task("New Task " + System.currentTimeMillis()));
    }

    public void save(final Task Task) {
        tasks.put(Task.getId(), Task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(final String id) {
        return tasks.get(id);
    }

    public void removeById(final String id) {
        tasks.remove(id);
    }

}