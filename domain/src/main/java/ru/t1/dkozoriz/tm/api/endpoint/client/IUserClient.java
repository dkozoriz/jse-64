package ru.t1.dkozoriz.tm.api.endpoint.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.IEndpointClient;
import ru.t1.dkozoriz.tm.dto.request.user.*;
import ru.t1.dkozoriz.tm.dto.response.user.*;

public interface IUserClient extends IEndpointClient {

    @NotNull
    LockUserResponse userLock(@NotNull UserLockRequest request);

    @NotNull
    UnlockUserResponse userUnlock(@NotNull UserUnlockRequest request);

    @NotNull
    RemoveUserResponse userRemove(@NotNull UserRemoveRequest request);

    @NotNull
    ChangePasswordUserResponse userChangePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UpdateProfileUserResponse userUpdateProfile(@NotNull UserUpdateProfileRequest request);

    @NotNull
    RegistryUserResponse userRegistry(@NotNull UserRegistryRequest request);

}