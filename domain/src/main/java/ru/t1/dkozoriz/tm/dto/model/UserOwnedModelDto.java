package ru.t1.dkozoriz.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class UserOwnedModelDto extends AbstractModelDto {

    @Nullable
    @Column(name = "user_id")
    private String userId;

}
